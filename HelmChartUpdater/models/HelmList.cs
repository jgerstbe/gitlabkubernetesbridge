﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json.Serialization;
using System.Threading.Tasks;

namespace HelmChartUpdater.models
{
    /// <summary>
    /// Model for helm list JSON output
    /// </summary>
    public class HelmList
    {
        [JsonPropertyName("name")]
        public string Name { get; set; }

        [JsonPropertyName("updated")]
        public string Updated { get; set; }

        [JsonPropertyName("status")]
        public string Status { get; set; }

        [JsonPropertyName("chart")]
        public string ChartVersion { get; set; }

        [JsonPropertyName("app_version")]
        public string AppVersion { get; set; }
    }
}
