export * from './account.service';
import { AccountService } from './account.service';
export * from './customProjects.service';
import { CustomProjectsService } from './customProjects.service';
export * from './gitlab.service';
import { GitlabService } from './gitlab.service';
export * from './kubernetes.service';
import { KubernetesService } from './kubernetes.service';
export const APIS = [AccountService, CustomProjectsService, GitlabService, KubernetesService];
