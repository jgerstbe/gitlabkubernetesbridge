using GitlabKubernetesBridge.Server.Helper;
using GitlabKubernetesBridge.Server.Models.GitlabAPI;
using k8s;

namespace GitlabKubernetesBridge.Migrations
{
    public class Migrate_from_v1_to_GKA
    {
        public static void Migrate(ILogger logger)
        {
            //enable project
            //create kube integration
            // Load from in-cluster configuration:
            var config = KubernetesClientConfiguration.InClusterConfig();

            // Use the config object to create a client.
            var client = new Kubernetes(config);

            string ownNamespace = File.ReadAllText("/var/run/secrets/kubernetes.io/serviceaccount/namespace");

            try
            {
                client.ReadNamespacedConfigMap("migrate-from-v1-to-gka-complete", ownNamespace);
                //Migration already done
                return;
            }
            catch (Exception)
            {
                logger.LogInformation($"Did not find migrate-from-v1-to-gka-complete Config Map in {ownNamespace} therefore expecting migration not allready done.");
            }

            logger.LogInformation("Start migrating old projects to use Gitlab Kubernetes Agent");
            logger.LogInformation("see https://about.gitlab.com/blog/2021/11/15/deprecating-the-cert-based-kubernetes-integration/");

            try
            {
                var gitlabApiClient = new GitlabApiClient(Environment.GetEnvironmentVariable("GITLAB_MANAGE_TOKEN"));

                var configuredNamespaces = client.ListNamespace(labelSelector: Org.OpenAPITools.Controllers.GitlabApiController.NamespaceProjectLabelName);
                foreach (var ns in configuredNamespaces.Items)
                {
                    logger.LogInformation($"Migrating {ns.Metadata.Name}");

                    string? ids = "";
                    int id;
                    ns.Metadata.Labels.TryGetValue(Org.OpenAPITools.Controllers.GitlabApiController.NamespaceProjectLabelName, out ids);
                    if (ids == null || ids == "" || !int.TryParse(ids, out id))
                    {
                        logger.LogError($"Coud not get Gitlab project from {Org.OpenAPITools.Controllers.GitlabApiController.NamespaceProjectLabelName} namespace label. Namespace: {ns.Metadata.Name}");
                        continue;
                    }

                    try
                    {
                        ProjectDetails project = gitlabApiClient.MakeGet<GitlabKubernetesBridge.Server.Models.GitlabAPI.ProjectDetails>("projects/" + id, new Dictionary<string, string>() { });

                        //delete Gitlab Managed Apps Rolebindings because they are not longer required.
                        try
                        {
                            client.DeleteNamespacedRoleBinding(ns.Metadata.Name + "-rolebinding", "gitlab-managed-apps");
                        }
                        catch (Exception ex)
                        {
                            logger?.LogError(ex, $"Failed to delete gitlab-managed-apps rolebinding for {ns.Metadata.Name}");
                        }

                        logger?.LogInformation($"Set Auto DevOps CI/CD Variables for {ns.Metadata.Name}");
                        Org.OpenAPITools.Controllers.GitlabApiController.SetCI_CD_Variable(gitlabApiClient, Org.OpenAPITools.Controllers.GitlabApiController.GitlabKubeNamespaceVariableName, ns.Metadata.Name, id, logger);
                        Org.OpenAPITools.Controllers.GitlabApiController.SetCI_CD_Variable(
                            gitlabApiClient,
                            Org.OpenAPITools.Controllers.GitlabApiController.GitlabKubeContextVariableName,
                            project.PathWithNamespace + ":" + Org.OpenAPITools.Controllers.GitlabApiController.GitlabKubeContextName,
                            id,
                            logger);

                        logger.LogInformation($"Register Gitlab Kubernetes Agent for {ns.Metadata.Name}");
                        int agentId;
                        try
                        {
                            agentId = gitlabApiClient.MakePost<GetGitlabAgent>($"projects/{id}/cluster_agents", new GitlabAgent
                            {
                                Name = Org.OpenAPITools.Controllers.GitlabApiController.GitlabKubeContextName
                            }).Id;
                        }
                        catch (Exception)
                        {
                            agentId = (from ga in gitlabApiClient.MakeGet<List<GetGitlabAgent>>($"projects/{id}/cluster_agents", new Dictionary<string, string>())
                                       where ga.Name == Org.OpenAPITools.Controllers.GitlabApiController.GitlabKubeContextName
                                       select ga.Id).First();
                        }

                        GetGitlabAgentToken agentToken;
                        try
                        {
                            agentToken = gitlabApiClient.MakePost<GetGitlabAgentToken>($"projects/{id}/cluster_agents/{agentId}/tokens", new GitlabAgentToken
                            {
                                Name = Org.OpenAPITools.Controllers.GitlabApiController.GitlabKubeContextName + "-token"
                            });
                        }
                        catch (Exception)
                        {
                            var agentTokens = gitlabApiClient.MakeGet<List<GetGitlabAgentToken>>($"projects/{id}/cluster_agents/{agentId}/tokens", new Dictionary<string, string>());
                            foreach (var token in agentTokens)
                            {
                                try
                                {
                                    gitlabApiClient.MakeDelete<object>($"projects/{id}/cluster_agents/{agentId}/tokens/{token.Id}");
                                }
                                catch (Exception ex)
                                {
                                    logger.LogError(ex, $"Failed to revoke Gitlab Kuberntes Agent token with name {token.Name} (ID: {token.Id}) for project id {id}");
                                }
                            }
                            agentToken = gitlabApiClient.MakePost<GetGitlabAgentToken>($"projects/{id}/cluster_agents/{agentId}/tokens", new GitlabAgentToken
                            {
                                Name = Org.OpenAPITools.Controllers.GitlabApiController.GitlabKubeContextName + "-token"
                            });
                        }


                        logger.LogInformation($"Install Gitlab Kubernets Agent for {ns.Metadata.Name}");
                        Org.OpenAPITools.Controllers.GitlabApiController.InstallGitlabKubernetesAgent(ns.Metadata.Name, agentToken.Token, logger);
                    }
                    catch (Exception ex)
                    {
                        logger.LogError(ex, $"Failed to migrate {ns.Metadata.Name} and Gitlab project id {id}");
                    }
                }

                logger.LogInformation("Migration to Gitlab Kubernetes Agent done. Please review logs for errors.");

                var completeFlag = new k8s.Models.V1ConfigMap()
                {
                    Metadata = new k8s.Models.V1ObjectMeta()
                    {
                        NamespaceProperty = ownNamespace,
                        Name = "migrate-from-v1-to-gka-complete"
                    },
                    Data = new Dictionary<string,string>()
                };
                client.CreateNamespacedConfigMap(completeFlag, ownNamespace);
            }
            catch (Exception ex)
            {
                logger.LogError(ex, "Migration to Gitlab Kubernetes Agent failed. Please try again. To trigger the migration process, you may delete migrate-from-v1-to-gka-complete Config Map.");
            }
        }
    }
}
