/*
 * API description for gitlab-kubernetes-bridge
 *
 * No description provided (generated by Openapi Generator https://github.com/openapitools/openapi-generator)
 *
 * The version of the OpenAPI document: 1.0.0
 * 
 * Generated by: https://openapi-generator.tech
 */

using System.Text;
using System.Text.Json;
using System.Text.Json.Serialization;

namespace GitlabKubernetesBridge.Server.Models.GitlabAPI
{
    /// <summary>
    /// 
    /// </summary>

    public partial class GetGitlabAgentTokenGraphQL : IEquatable<GetGitlabAgentTokenGraphQL>
    {
        /// <summary>
        /// Name of the token
        /// </summary>
        [JsonPropertyName("data")]
        public GetGitlabAgentTokenGraphQLTokenCreate data { get; set; }

        public class GetGitlabAgentTokenGraphQLTokenCreate
        {
            [JsonPropertyName("clusterAgentTokenCreate")]
            public GetGitlabAgentTokenGraphQLTokenCreateSecret clusterAgentTokenCreate { get; set; }
        }

        public class GetGitlabAgentTokenGraphQLTokenCreateSecret
        {
            [JsonPropertyName("secret")]
            public string secret { get; set; }
        }


        

        /// <summary>
        /// Returns the string presentation of the object
        /// </summary>
        /// <returns>String presentation of the object</returns>
        public override string ToString()
        {
            var sb = new StringBuilder();
            sb.Append("class ProjectVariable {\n");
            sb.Append("  data: ").Append(this.data).Append("\n");
            sb.Append("}\n");
            return sb.ToString();
        }

        /// <summary>
        /// Returns the JSON string presentation of the object
        /// </summary>
        /// <returns>JSON string presentation of the object</returns>
        public string ToJson()
        {
            return JsonSerializer.Serialize(this, new JsonSerializerOptions { WriteIndented = true });
        }

        /// <summary>
        /// Returns true if objects are equal
        /// </summary>
        /// <param name="obj">Object to be compared</param>
        /// <returns>Boolean</returns>
        public override bool Equals(object obj)
        {
            if (obj is null) return false;
            if (ReferenceEquals(this, obj)) return true;
            return obj.GetType() == GetType() && Equals((GetGitlabAgentToken)obj);
        }

        /// <summary>
        /// Returns true if GitlabProject instances are equal
        /// </summary>
        /// <param name="other">Instance of GitlabProject to be compared</param>
        /// <returns>Boolean</returns>
        public bool Equals(GetGitlabAgentTokenGraphQL other)
        {
            if (other is null) return false;
            if (ReferenceEquals(this, other)) return true;

            return
                (
                    this.data == other.data ||
                    this.data != null &&
                    this.data.Equals(other.data)
                );
        }


    }
}
