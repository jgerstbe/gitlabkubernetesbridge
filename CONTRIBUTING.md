# Development and Contribution Guide

🔥 You are welcome to contribute any bug fix, enhancement etc. to Gitlab Kubernetes Bridge (or simply GKB).  

## How to start developing?
You will dotnet installed on your host, a Kubernetes Cluster for testing and a Gitlab instance, which can also be gitlab.com .
You can install minikube on your own computer or use any Kubernetes Cluster to connect GKB with. 
Set these environment variables in your launch settings to let GKB connect to your (test) Kubernetes cluster:  

|Variable| Content|
|--------|--------|
|`KUBERNETES_SERVICE_PORT`| Your Kubernetes API port of your cluster. Use `8443` if you use minikube.|
|`KUBERNETES_SERVICE_HOST`| Your Kubernetes API host FQDN/IP of your cluster. Use `127.0.0.1` if you use minikube.|

You also have to create the following files because GKB is designed to run inside a Kubernetes Pod therefore it assumes that some files exists:  

|Filepath|Content|
|--------|-------|
|`/var/run/secrets/kubernetes.io/serviceaccount/token`|This file should contain a Kubernetes serviceaccount token with cluster-admin rights in plain text. E.g. see https://medium.com/@kanrangsan/creating-admin-user-to-access-kubernetes-dashboard-723d6c9764e4 to create one.|
|`/var/run/secrets/kubernetes.io/serviceaccount/ca.crt`|This file should contain the public Kubernetes CA certificate of your cluster. Simply copy `~/.minikube/ca.crt` if you are using minikube.|

To connect your Development environment to Gitlab follow the instructions in [Readme.md](Readme.md).

## How to contribute your changes?
First of all, please open an issue and describe what is going wrong, or what do you want to enhance.
Create a merge request referencing your issue and describe what did you exactly changed in the code and why.
We will do our best to give you feedback as quick as possible.
